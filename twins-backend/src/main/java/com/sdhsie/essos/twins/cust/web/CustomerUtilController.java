package com.sdhsie.essos.twins.cust.web;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.sdhs.xlpayment.utils.encrypt.RSAUtils;
import com.sdhsie.asoiaf.core.exception.ApplicationException;
import com.sdhsie.asoiaf.core.exception.UnAuthorizedException;
import com.sdhsie.essos.twins.base.BaseAuthController;
import com.sdhsie.essos.twins.base.ConstantUtil;
import com.sdhsie.essos.twins.cust.service.CustService;
import com.sdhsie.essos.twins.cust.service.YunPianSmsService;
import com.sdhsie.essos.twins.util.KeysUtil;
import com.sdhsie.essos.twins.util.StringUtils;
import com.sdhsie.essos.twins.util.VerifyCodeUtils;
import com.sdhsie.westeros.kingslanding.cust.model.CustDicking;
import com.sdhsie.westeros.kingslanding.cust.model.Customer;
import com.sdhsie.westeros.kingslanding.cust.service.AppCooperationService;
import com.sdhsie.westeros.kingslanding.cust.service.CustDickingService;
import com.sdhsie.westeros.kingslanding.cust.service.CustomerService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

/**
 * Created by huangshuai on 2017/7/4.
 */

@Controller
@RequestMapping("/cust")
public class CustomerUtilController extends BaseAuthController {

    @Autowired
    CustomerService customerService;
    @Autowired
    AppCooperationService appCooperationService;
    @Autowired
    CustDickingService custDickingService;
    @Autowired
    YunPianSmsService yunPianSmsService;
    @Autowired
    CustDickingService dickingService;

    /**
     * 用户登陆
     */
    @ResponseBody
    @RequestMapping(value = "/authorize", params = {"login_name", "login_password"}, method = {RequestMethod.POST})
    public Object authorize( String login_name, String login_password,HttpServletRequest request){
        Map<String,Object> result = Maps.newHashMap();
        //获取header中的appKey
        String appKey = request.getHeader("appKey");
        //首先验证注册用户是否有appKey
        if(appCooperationService.checkAppKey(appKey)){
            //有合法的appKey，可以进行注册操作
            Customer customer = customerService.getByMobilePhone(login_name);
            if(customer == null){
                customer = customerService.getByNickName(login_name);
                if(customer == null){
                    logger.error("系统无此用户: " + login_name);
                    result.put("code","401");
                    result.put("msg","系统无此用户");
                }
                return result;
            }else if(!customerService.validate(customer.getMobilePhone(), login_password)){
                logger.error("用户密码输入错误:" + login_name);
                result.put("code","402");
                result.put("msg","用户密码输入错误");
                return result;
            }else{
                //登陆成功
                Map<String,Object> map = custDickingService.getAuthorEncryptCode(login_name);
                if(map != null){
                    result.put("encryptcode",map.get("encryptcode"));
                    result.put("nickName",map.get("nickName"));
                }
                result.put("code","200");
                result.put("msg","登陆成功");
                return result;
            }
        }else{
            //没有合法的appKey
            result.put("failCode","noKeys");
            return result;
        }
    }

    /**
     * 注册第一步
     * 注册时，先发送手机验证码
     */
    @ResponseBody
    @RequestMapping(value = "/registerMobile", params = {"mobilePhone"}, method = {RequestMethod.POST})
    public Object registerMobile(String mobilePhone,HttpServletRequest request){
        Map<String,Object> result = Maps.newHashMap();
        //获取header中的appKey
        String appKey = request.getHeader("appKey");
        if(appCooperationService.checkAppKey(appKey)){
            HttpSession session = getSession();
            LocalDateTime reSendTime = (LocalDateTime)session.getAttribute("Register.verificationCodeReSendTime");
            //验证手机
            Customer customer = customerService.getByMobilePhone(mobilePhone);
            if(reSendTime != null && reSendTime.isAfter(LocalDateTime.now())){
                result.put("code","503");
                result.put("msg","验证码发送过频");

            }else if(Strings.isNullOrEmpty(mobilePhone)){
                result.put("code","501");
                result.put("msg","手机号无效");

            }else if(customer != null){
                result.put("code","502");
                result.put("msg","手机号已注册");
            }else{
                //生成验证码
                String verificationCode = VerifyCodeUtils.generateVerificationCode();
                //result.put("verification_code",verificationCode);
                session.setAttribute("Register.verificationMobilePhone",mobilePhone);
                session.setAttribute("Register.verificationCode",verificationCode);
                //发送短信
                yunPianSmsService.sendRegisterVerificationCode(mobilePhone, verificationCode);
                LocalDateTime dateTime = LocalDateTime.now();
                dateTime = dateTime.plusMinutes(5);
                result.put("expires_in", Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant()));
                session.setAttribute("Register.verificationCodeExpiresIn",dateTime);
                session.setAttribute("Register.verificationCodeReSendTime",LocalDateTime.now().plusMinutes(1));
                result.put("code","200");
                result.put("msg","发生成功");
            }
        }else {
            //没有合法的appKey
            result.put("failCode","noKeys");
        }
        return result;
    }

    /**
     * 注册第二步
     * 验证手机收到的验证码
     * @param verificationCode
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/registerVerification", params = {"verificationCode"}, method = {RequestMethod.POST})
    public Object registerVerification(String verificationCode,HttpServletRequest request){
        HttpSession session = getSession();
        Map<String,Object> result = Maps.newHashMap();
        String appKey = request.getHeader("appKey");
        if(appCooperationService.checkAppKey(appKey)){
            String code = (String)session.getAttribute("Register.verificationCode");
            LocalDateTime dateTime = (LocalDateTime)session.getAttribute("Register.verificationCodeExpiresIn");
            if(code == null){
                result.put("code","505");
                result.put("msg","未发送验证码");
            }else if(dateTime != null && dateTime.isBefore(LocalDateTime.now())){
                result.put("code","504");
                result.put("msg","验证码过期");
            }else if(!code.equals(verificationCode)){
                logger.error("验证码无效 code: {}, inputCode: {}", code, verificationCode);
                result.put("code","405");
                result.put("msg","验证码无效");
            }else{
                session.setAttribute("Register.verificationCodeSuccess",true);
                result.put("verification","success");
                String mobilePhone = (String)session.getAttribute("Register.verificationMobilePhone");
                logger.info("手机(" + mobilePhone + ")验证码认证成功:" + verificationCode);
                result.put("code","200");
                result.put("msg","验证码认证成功");
            }
        }else{
            //没有合法的appKey
            result.put("failCode","noKeys");
        }
        return result;
    }

    /**
     * 注册第三步
     * 注册信息，填写昵称与密码
     * @param nikeName
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/registerSet",params = {"password","nikeName","wxKey","aliPayKey","source"},method = {RequestMethod.POST})
    public Object registerSet(String nikeName, String password,String wxKey,
                              String aliPayKey,String source,
                              HttpServletRequest request){
        HttpSession session = getSession();
        Map<String,Object> result = Maps.newHashMap();
        String appKey = request.getHeader("appKey");
        if(appCooperationService.checkAppKey(appKey)){
            String mobilePhone = (String)session.getAttribute("Register.verificationMobilePhone");
            Customer customer = customerService.getByMobilePhone(mobilePhone);
            customer = customerService.getByNickName(nikeName);
            if(Strings.isNullOrEmpty(nikeName)){
                result.put("code","506");
                result.put("msg","用户名为空");
            }else if(Strings.isNullOrEmpty(password)){
                result.put("msg","密码为空");
                result.put("code","507");
            }else if(session.getAttribute("Register.verificationCodeSuccess") == null){
                result.put("msg","请先验证手机");
                result.put("code","508");
            }else if(customer != null){
                logger.error("手机号已注册: ", mobilePhone);
                result.put("msg","手机号已注册");
                result.put("code","502");
            }else if(customer != null){
                result.put("msg","用户名:" + nikeName + "已经被注册，请重新输入。");
                result.put("code","509");
            }else{
                try{
                    //加入用户注册信息，cust_cust表
                    customer = new Customer();
                    customer.setMobilePhone(mobilePhone);
                    customer.setLoginPassword(password);
                    customer.setNickName(nikeName);
                    customerService.add(customer);
                    //添加信息，返回主键
//                    customerService.addCallBack(customer);
                    //清空验证状态
                    session.setAttribute("Register.verificationCodeSuccess",null);
                    //向cust_docking表中，插入用户信息
                    CustDicking custDick = new CustDicking();
                    if(aliPayKey != null && aliPayKey.length() > 0){
                        custDick.setAliPayKey(aliPayKey);
                    }
                    if(wxKey != null && wxKey.length() > 0){
                        custDick.setWxKey(wxKey);
                    }
                    if(source != null && source.length() > 0){
                        custDick.setSource(source);
                    }
                    custDick.setAppKey(appKey);
                    custDick.setCustId(customerService.getByMobilePhone(mobilePhone).getId());
                    //生成encryptcode，然后验证，如果重复了则重新生成
                    String encryptcode = KeysUtil.createEncryptCode(appKey,customerService.getByMobilePhone(mobilePhone).getId());
                    if(custDickingService.isEncryptCode(encryptcode)){
                        //有重复的，重新生成
                        custDick.setEncryptcode(KeysUtil.createEncryptCode(appKey,customerService.getByMobilePhone(mobilePhone).getId()));
                    }else{
                        custDick.setEncryptcode(encryptcode);
                    }
                    custDickingService.add(custDick);
                    result.put("code","200");
                    result.put("msg","用户注册成功");
                    logger.info("用户注册成功:" + mobilePhone + ":" + nikeName);
                    //新用户注册成功，邀请人与被邀请人生成红包兑换码
//                BasicInfo basicInfo = basicInfoService.getActivityInfo();
//                if(basicInfo != null){
//                    activitisService.redeemCode(mobilePhone,getDeviceId());
//                }
                }catch (Exception e){
                    result.put("code","500");
                    result.put("msg","系统繁忙，请稍后");
                }
            }
        }else{
            //没有合法的appKey
            result.put("msg","noKeys");
        }
        return result;
    }

    /**
     * 获取用户信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/findUserInfo", params = {"params"}, method = {RequestMethod.POST})
    public Object registerOrLogin(String params,HttpServletRequest request){
        Map<String,String> m = Maps.newHashMap();
        JSONObject jsonObj = new JSONObject();
        String result = "";
        logger.error("appKey："+request.getHeader("appKey"));
        logger.error("params："+params);
        try{
            //获取header中的appKey，首先需要先对齐进行解密
            String appKey = RSAUtils.decrypt(request.getHeader("appKey"), ConstantUtil.PRIVATE_KEY);
            //首先验证注册用户是否有appKey
            if(appCooperationService.checkAppKey(appKey)){
                if(StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY)).get("mobilePhone") != null){
                    String newMobilePhone = StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY)).get("mobilePhone").toString();
                    //有合法的appKey，可以进行注册操作，先看是否有该用户
                    Customer customer = customerService.getByMobilePhone(newMobilePhone);
                    if(customer != null){
                        m.put("nickName", customer.getNickName());
                        m.put("mobilePhone", customer.getMobilePhone());
                        jsonObj.put("code","200");
                        jsonObj.put("desc","获取成功");
                        jsonObj.put("data",m);
                    }else{
                        jsonObj.put("code","404");
                        jsonObj.put("desc","没有该手机的注册信息");
                        jsonObj.put("data","");
                    }
                }else if(StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY)).get("encryptCode") != null){
                    String encryptCode = StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY)).get("encryptCode").toString();
                    Map<String, Object> map = dickingService.getByEncryptcode(encryptCode);
                    if(map != null){
                        m.put("mobilePhone", map.get("mobilePhone").toString());
                        m.put("nickName",map.get("nickName").toString());
                        jsonObj.put("code","200");
                        jsonObj.put("desc","获取成功");
                        jsonObj.put("data",m);
                    }else{
                        jsonObj.put("code","404");
                        jsonObj.put("desc","没有该手机的注册信息");
                        jsonObj.put("data","");
                    }
                }else{
                    jsonObj.put("code","500");
                    jsonObj.put("desc","参数传入错误！");
                    jsonObj.put("data","");
                }
                result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
                return result;
            }else{
                //没有合法的appKey
                jsonObj.put("code","407");
                jsonObj.put("desc","无效的Key");
                jsonObj.put("data","");
                result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
                return result;
            }
        }catch (Exception e){
            jsonObj.put("code","500");
            jsonObj.put("desc","系统繁忙，请稍后");
            jsonObj.put("data","");
            logger.error("获取用户信息出现异常：" + e);
            try {
                result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return result;
        }
    }


}
