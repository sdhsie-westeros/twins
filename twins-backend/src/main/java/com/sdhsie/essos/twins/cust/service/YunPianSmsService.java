package com.sdhsie.essos.twins.cust.service;

import com.sdhsie.westeros.kingslanding.cust.model.SmsHist;
import com.sdhsie.westeros.kingslanding.cust.service.SmsHistService;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [云片网络]短信服务
 */
@Service("WFSmsYunPianService")
public class YunPianSmsService {
    Logger logger = LoggerFactory.getLogger(getClass());

    private static String URI_SEND_SMS = "http://yunpian.com/v1/sms/send.json";
    private static String ENCODING = "UTF-8";
    private static String APIKEY = "9b92bc512ec082bc439e313a19f1e505"; // E
    private static String SIGNATURE = "【e高速】";

    @Autowired
    SmsHistService smsHistService;

    /**
     * 给用户发送通知
     * @param mobilePhone
     * @param content
     */
    public void sendNodeToCust(String mobilePhone,String content){
        sendSms(mobilePhone, content);
    }

    public void sendRegisterVerificationCode(String mobilePhone, String verificationCode){
        String content = "您的注册验证码为："+verificationCode+"，请完成验证，如非本人操作，请忽略本短信。";
        sendSms(mobilePhone, content);
    }

    public void sendForgetPasswordVerificationCode(String mobilePhone, String verificationCode){
        String content = "您修改密码的验证码为："+verificationCode+"，如非本人操作，请注意保护您的账户安全。";
        sendSms(mobilePhone, content);
    }

    public static void main(String[] args) {
        YunPianSmsService smsYunPianService = new YunPianSmsService();
        smsYunPianService.sendForgetPasswordVerificationCode("18663702505", "8888");
    }

    /**
     * 随机密码短信
     * @param mobilePhone
     * @param randomPwd
     */
    public void sendRandomPwd(String mobilePhone, String randomPwd){
        String content = "欢迎注册e高速APP，您的随机密码为："+randomPwd+"，请妥善保管，如需修改请登陆e高速APP。";
        logger.error("内容："+content);
        sendSms(mobilePhone, content);
    }

    /**
     * 通用接口发短信
     *
     * @param text   　短信内容
     * @param mobile 　接受的手机号
     * @return json格式字符串
     */
    public String sendSms(String mobile, String text) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("apikey", APIKEY);
        params.put("mobile", mobile);
        params.put("text", SIGNATURE + text);
        String returnCode = post(URI_SEND_SMS, params);
        logger.error(returnCode+"returnCode");
//        System.out.println(returnCode);
        // Insert History
        SmsHist smsHist = new SmsHist();
        smsHist.setMobilePhone(mobile);
        smsHist.setContent(text);
        smsHist.setReturnCode(returnCode);
//        smsHistService.add(smsHist);
        return returnCode;
    }

    /**
     * 基于HttpClient 4.3的通用POST方法
     *
     * @param url       提交的URL
     * @param paramsMap 提交<参数，值>Map
     * @return 提交响应
     */
    public String post(String url, Map<String, String> paramsMap) {
        CloseableHttpClient client = HttpClients.createDefault();
        String responseText = "";
        CloseableHttpResponse response = null;
        try {
            HttpPost method = new HttpPost(url);
            if (paramsMap != null) {
                List<NameValuePair> paramList = new ArrayList<NameValuePair>();
                for (Map.Entry<String, String> param : paramsMap.entrySet()) {
                    NameValuePair pair = new BasicNameValuePair(param.getKey(), param.getValue());
                    paramList.add(pair);
                }
                method.setEntity(new UrlEncodedFormEntity(paramList, ENCODING));
            }
            response = client.execute(method);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                responseText = EntityUtils.toString(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(),e);
        } finally {
            try {
                response.close();
            } catch (Exception e) {
                e.printStackTrace();
                logger.error(e.getMessage(),e);
            }
        }
        logger.debug(responseText);
        return responseText;
    }

}
