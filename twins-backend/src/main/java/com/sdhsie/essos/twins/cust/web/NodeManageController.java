package com.sdhsie.essos.twins.cust.web;

import com.sdhs.xlpayment.utils.encrypt.RSAUtils;
import com.sdhsie.essos.twins.base.BaseAuthController;
import com.sdhsie.essos.twins.base.ConstantUtil;
import com.sdhsie.essos.twins.cust.service.ProjectPush;
import com.sdhsie.essos.twins.cust.service.YunPianSmsService;
import com.sdhsie.essos.twins.util.StringUtils;
import com.sdhsie.essos.twins.util.TimeUtil;
import com.sdhsie.westeros.kingslanding.cust.model.CustNodeSendRecord;
import com.sdhsie.westeros.kingslanding.cust.service.AppCooperationService;
import com.sdhsie.westeros.kingslanding.cust.service.CustDickingService;
import com.sdhsie.westeros.kingslanding.cust.service.CustNodeSendRecordService;
import com.sdhsie.westeros.kingslanding.device.model.Device;
import com.sdhsie.westeros.kingslanding.device.service.DeviceService;
import com.sdhsie.westeros.kingslanding.roadcondition.model.PushedMessage;
import com.sdhsie.westeros.kingslanding.roadcondition.service.PushedMessageService;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by huangshuai on 2017/7/17.
 * 2.通知接口
 */

@Controller
@RequestMapping("/nodeController")
public class NodeManageController extends BaseAuthController {


    public static final String SERVICE_TYPE_PROJECTPUSH = "projectpush";
    public static final String IOS_XINGE_DEVICETOKEN_PREFIX = "xinge-";
    ProjectPush projectPush = new ProjectPush();
    Logger logger = LoggerFactory.getLogger(getClass());
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    AppCooperationService appCooperationService;
    @Autowired
    CustDickingService dickingService;
    @Autowired
    CustNodeSendRecordService custNodeSendRecordService;
    @Autowired
    YunPianSmsService yunPianSmsService;

    @Autowired
    DeviceService deviceService;
    @Autowired
    PushedMessageService pushedMessageService;

    @ResponseBody
    @RequestMapping(value = "/sendOrPush", params = {"params"}, method = {RequestMethod.POST})
    public Object sendOrPush(String params,HttpServletRequest request){
        String result = "";
        JSONObject jsonObj = new JSONObject();
        logger.error("appKey："+request.getHeader("appKey"));
        logger.error("params："+params);
        try{
            //获取header中的appKey，首先需要先对齐进行解密
            String appKey = RSAUtils.decrypt(request.getHeader("appKey"), ConstantUtil.PRIVATE_KEY);
            Map<String,Object> paramsMap = StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY));
            //解密发送内容
            String content = paramsMap.get("content").toString();
            logger.error("content: "+content);
            //解密推送级别，0：手机和短信推送1：手机推送，2：短信推送
            Integer pushLevel = Integer.valueOf(paramsMap.get("pushLevel").toString());
            //首先验证注册用户是否有appKey
            if(appCooperationService.checkAppKey(appKey)){
                Map<String,Object> c = dickingService.getByEncryptcode(paramsMap.get("encryptcode").toString());
                if(c != null){
                    //根据推送的级别，进行操作
                    if(pushLevel == 1){
//                        //手机推送
                        projectPush(Long.parseLong(c.get("custId").toString()), content, Long.parseLong(c.get("id").toString()));
                    }else if(pushLevel == 2){
                        //短信推送
                        yunPianSmsService.sendNodeToCust(c.get("mobilePhone").toString(),content);
                    }else if(pushLevel == 0){
                        //手机推送
                        projectPush(Long.parseLong(c.get("custId").toString()), content, Long.parseLong(c.get("id").toString()));
                        //短信推送
                        yunPianSmsService.sendNodeToCust(c.get("mobilePhone").toString(),content);
                    }
                    //将发送信息的记录，进行存储
                    CustNodeSendRecord custNodeSendRecord = new CustNodeSendRecord();
                    custNodeSendRecord.setContent(content);
                    custNodeSendRecord.setCustId(Long.valueOf(c.get("custId").toString()));
                    Date date = sdf.parse(TimeUtil.stampToDate(paramsMap.get("occurTime").toString()));
                    custNodeSendRecord.setOccurTime(date);
                    custNodeSendRecord.setPushType(Integer.valueOf(paramsMap.get("pushType").toString()));
                    custNodeSendRecord.setPushLevel(pushLevel);
                    custNodeSendRecordService.add(custNodeSendRecord);
                    jsonObj.put("code","200");
                    jsonObj.put("desc","操作成功");
                    jsonObj.put("data","");
                    result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
                    return result;
                }else{
                    //没有该用户
                    jsonObj.put("code","409");
                    jsonObj.put("desc","无效的用户");
                    jsonObj.put("data","");
                    result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
                }
            }else{
                //没有合法的appKey
                jsonObj.put("code","407");
                jsonObj.put("desc","无效的Key");
                jsonObj.put("data","");
                result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
                System.out.println();
            }
        }catch (Exception e){
            jsonObj.put("code","500");
            jsonObj.put("desc","系统繁忙，请稍后");
            jsonObj.put("data","");
            logger.error("快速登陆出现异常：" + e);
            try {
                result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        System.out.println("result: "+result);
        return result;
    }

    /**
     * 进行推送
     * */
    public void projectPush(Long custId, String content, Long owcId){
        //根据用户Id查询负责人的设备信息
        List<Device> devices = deviceService.findDevicesByCustId(custId);
        if(devices != null && devices.size() > 0){
            for(Device device : devices){
                if (org.springframework.util.StringUtils.isEmpty(device.getOs()) || org.springframework.util.StringUtils.isEmpty(device.getPushToken())) {
                    continue;
                }
                String pushResult = "";
                if("ios".equals(device.getOs())){
                    // PushToken[xinge-]开头，则使用信鸽推送
                    if (device.getPushToken().startsWith(IOS_XINGE_DEVICETOKEN_PREFIX)) {
                        pushResult = projectPush.pushProjectXinge(device.getPushToken().replace(IOS_XINGE_DEVICETOKEN_PREFIX, ""), String.valueOf(owcId), content, device.getAppVersion(), false);
                    } else {
                        // 其他的PushToken使用APNS
//                        projectPush.pushProjectApns(device.getPushToken(), String.valueOf(owcId), content, false);
//                        pushResult = "APNS";
                    }
                }if("android".equals(device.getOs())){
                    // 如果当前设备的APPVersion不为空，则使用透传，否则使用通知
                    boolean isMessage = !org.springframework.util.StringUtils.isEmpty(device.getAppVersion());
                    pushResult = projectPush.pushProjectAndroid(device.getPushToken(), String.valueOf(owcId), content, false, isMessage);
                }
                // 记录此次推送以便日后统计
                PushedMessage pm = new PushedMessage(custId, device.getId(), device.getOs(), device.getPushToken(), SERVICE_TYPE_PROJECTPUSH, String.valueOf(owcId), content, false, pushResult);
                pushedMessageService.add(pm);
            }
        }
    }
}
