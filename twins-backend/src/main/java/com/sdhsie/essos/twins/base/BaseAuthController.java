package com.sdhsie.essos.twins.base;


import com.sdhsie.asoiaf.core.web.BaseController;
import com.sdhsie.westeros.kingslanding.cust.model.AccessToken;
import com.sdhsie.westeros.kingslanding.cust.model.Customer;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by limiemie on 2014/8/20.
 */
public class BaseAuthController extends BaseController {

    public static final String IMAGE_BASE_DIR = "/MOBILE";

    public static final String OS_ANDROID =  "ANDROID";
    public static final String OS_IOS = "IOS";

    public static final String ANDROID_HDPI = "HDPI";
    public static final String ANDROID_XHDPI = "XHDPI";
    public static final String ANDROID_XXHDPI = "XXHDPI";
    public static final String IOS_2X = "2X";
    public static final String IOS_3X = "3X";

    public static final String RESOLUTION_IPHONE6_PLUS = "2208*1242";

    public Customer getLoginCustomer(){
        return (Customer)getSession().getAttribute("Cust.customer");
    }

    public Long getLoginCustId(){
        Customer customer = getLoginCustomer();
        if(customer == null){
            return null;
        }
        return customer.getId();
    }

    public Long getDeviceId(){
        AccessToken accessToken = (AccessToken)getSession().getAttribute("Cust.accessToken");
        if(accessToken == null){
            return null;
        }
        return accessToken.getDeviceId();
    }

    /**
     * 根据用户手机的操作系统及DPI或者分辨率信息，获取该机型对应的图片尺寸前缀。
     * 如 : iPhone6Plus - /MOBILE/IOS/3X
     * 如 : MeizuMX4Pro - /MOBILE/ANDROID/XXHDPI
     * @return
     */
    public String getImagePrefix(String imageType) {
        String imagePrefix = "";

        String os = getRequest().getHeader("os");
        if(StringUtils.isEmpty(os)) {
            return imagePrefix;
        }
        if(OS_ANDROID.toLowerCase().equals(os.toLowerCase())) {
            imagePrefix = IMAGE_BASE_DIR + "/" + OS_ANDROID + "/" + StringUtils.defaultString(imageType);
            int dpi = getRequest().getIntHeader("dpi");
            if (dpi <= 240) {
                imagePrefix += "/" + ANDROID_HDPI;
            } else if (dpi <= 320) {
                imagePrefix += "/" + ANDROID_XHDPI;
            } else if (dpi <= 480) {
                imagePrefix += "/" + ANDROID_XXHDPI;
            } else {
                imagePrefix += "/" + ANDROID_XXHDPI;
            }
        } else if (OS_IOS.toLowerCase().equals(os.toLowerCase())) {
            imagePrefix = IMAGE_BASE_DIR + "/" + OS_IOS + "/" + StringUtils.defaultString(imageType);
            String resolution = getRequest().getHeader("resolution");
            if(RESOLUTION_IPHONE6_PLUS.equals(resolution)) {
                imagePrefix += "/" + IOS_3X;
            } else {
                imagePrefix += "/" + IOS_2X;
            }
        }

        return imagePrefix;
    }


}
