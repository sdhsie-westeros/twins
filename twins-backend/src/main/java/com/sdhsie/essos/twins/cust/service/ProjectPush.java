package com.sdhsie.essos.twins.cust.service;

import com.google.common.collect.Maps;
import com.relayrides.pushy.apns.PushManager;
import com.relayrides.pushy.apns.util.ApnsPayloadBuilder;
import com.relayrides.pushy.apns.util.SimpleApnsPushNotification;
import com.relayrides.pushy.apns.util.TokenUtil;
import com.xinge.*;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * Created by jinan-dg-20150525 on 2015/11/23.
 */
public class ProjectPush {
    Logger logger = LoggerFactory.getLogger(getClass());
    XingeApp xinge = new XingeApp(2100046537, "c811262d1b366dc1fd0150a624cc152d");
    PushManager<SimpleApnsPushNotification> pushManager;

    /**
     * 信鸽IOS推送
     * */
    public String pushProjectXinge(String deviceToken, String rcId, String content, String appVersion, boolean silence) {

        logger.debug("ios pushToken:{}, rcId:{}, content:{}, silence:{}", deviceToken,rcId, content, silence);

        MessageIOS message = new MessageIOS();

        if (!silence){
            message.setSound("default");
        }

        Map<String,Object> map = Maps.newHashMap();
        if(appVersion != null && appVersion.compareTo("2.1.3") >= 0){
            map.put("a","1");
            map.put("b", rcId);
        }else{
            map.put("serviceType","roadCondition");
            map.put("rcId", rcId);
        }
        message.setCustom(map);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bys = baos.toByteArray();
        int byteNum = 0;
        if(bys.length > 144){
            byteNum = 124 - (bys.length - 144);
        }else{
            byteNum = 124 + (144 - bys.length);
        }
        content = cutContentByByteNum(content, byteNum);
        message.setAlert(content);

        JSONObject result = xinge.pushSingleDevice(deviceToken, message, XingeApp.IOSENV_PROD);
        logger.debug("推送结果：{}", result);
        return result.toString();
    }

    public String cutContentByByteNum(String content, int byteNum){
        if (StringUtils.isEmpty(content)) {
            return "";
        }
        int contentByteNum=content.getBytes(Charset.defaultCharset()).length;
        StringBuffer resultContent = new StringBuffer();
        if(contentByteNum > byteNum){
            char itemContent[] = content.toCharArray();
            for(int i = 0 ;i < itemContent.length; i++){
                if((resultContent.toString().getBytes(Charset.defaultCharset()).length + String.valueOf(itemContent[i]).getBytes(Charset.defaultCharset()).length) <= byteNum - 3){
                    resultContent.append(itemContent[i]);
                }else{
                    resultContent.append("...");
                    break;
                }
            }
        }else{
            resultContent.append(content);
        }
        return resultContent.toString();
    }

    /**
     * APNS的IOS推送
     * */
    public String pushProjectApns(String deviceToken, String rcId, String content, boolean silence) {

        logger.debug("ios pushToken:{}, pId:{}, content:{}, silence:{}", deviceToken,rcId, content, silence);
        try{

            final byte[] token = TokenUtil.tokenStringToByteArray(deviceToken);

            final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();

            payloadBuilder.setAlertBody(content);
            if(!silence){
                payloadBuilder.setSoundFileName("default");

            }
            payloadBuilder.addCustomProperty("serviceType","projectPush");
            payloadBuilder.addCustomProperty("rcId",rcId);
            final String payload = payloadBuilder.buildWithDefaultMaximumLength();

            pushManager.getQueue().put(new SimpleApnsPushNotification(token, payload));
        }catch (Exception e){
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Android推送工程异常信息（透传方式 OR 通知方式）
     * @param deviceToken
     * @param content
     * @param silence
     * @return
     */
    public String pushProjectAndroid(String deviceToken, String rcId, String content, boolean silence, boolean isMessage) {

        logger.debug("android pushToken:{}, content:{}, silence:{}", deviceToken, content, silence);

        Message message = new Message();
        message.setTitle("e高速");
        message.setContent(content);
        if (isMessage) {
            // 透传
            message.setType(Message.TYPE_MESSAGE);
        } else {
            // 通知
            message.setType(Message.TYPE_NOTIFICATION);
        }

        int ring = 0;
        if (!silence) {
            ring = 1;
        }

        Style style = new Style(3, ring, 0, 1, 0);
        message.setStyle(style);
        // 通知
        if (!isMessage){
            ClickAction action = new ClickAction();
            action.setActionType(1);
            action.setActivity("com.sdhs.easy.high.road.MainActivity");
            message.setAction(action);
        }
        Map<String,Object> map = Maps.newHashMap();
        map.put("serviceType","projectPush");
        map.put("rcId",rcId);
        message.setCustom(map);
        JSONObject result = xinge.pushSingleDevice(deviceToken, message);
        return result.toString();
    }

    public static void main(String args[]){
        String deviceToken = "08dad3b003128ab124beaddfd2d6a232a31965ed8e0d89956f34416811b1218b";
        String rcId = "181";
        String content = "sdhs_clgz-5分钟，sdhs_dltz-5分钟，sdhs_jtgz-5分钟，sdhs_jtsg-5分钟，sdhs_sgxx-5分钟";
        String appVersion = "2.0.6";
        String result = new ProjectPush().pushProjectXinge(deviceToken, rcId, content, appVersion, false);
        System.out.println(result);
    }
}
