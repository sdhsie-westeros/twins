package com.sdhsie.essos.twins.cust.web;

import com.sdhs.xlpayment.utils.encrypt.RSAUtils;
import com.sdhsie.essos.twins.base.BaseAuthController;
import com.sdhsie.essos.twins.base.ConstantUtil;
import com.sdhsie.essos.twins.util.StringUtils;
import com.sdhsie.westeros.kingslanding.cust.service.AppCooperationService;
import com.sdhsie.westeros.kingslanding.cust.service.CustEncryptAutoService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 车险分期-对外输出接口
 */
@Controller
@RequestMapping(value = "/auto/insurance")
public class AutoInsuranceController extends BaseAuthController {
    @Autowired
    CustEncryptAutoService custEncryptAutoService;
    @Autowired
    AppCooperationService appCooperationService;

    @ResponseBody
    @RequestMapping(value = "/getUserInfo", method = RequestMethod.POST)
    public Object getUserInfo(String message, HttpServletRequest request) throws Exception {
        JSONObject jsonObj = new JSONObject();
        logger.error("============================车险分期====================================");
        logger.error("appKey："+request.getHeader("appKey"));
        logger.error("message："+message);
        try{
            //获取header中的appKey，首先需要先对齐进行解密
            String appKey = RSAUtils.decrypt(request.getHeader("appKey"), ConstantUtil.PRIVATE_KEY);
            //首先验证注册用户是否有appKey
            if(appCooperationService.checkAppKey(appKey)){
                //解密encryptCode
                message = RSAUtils.decrypt(message, ConstantUtil.PRIVATE_KEY);
                String encryptCode = ((Map<String, Object>)StringUtils.parseJSON2Map(message).get("params")).get("encryptCode").toString();
                Map<String, Object> map = custEncryptAutoService.getByEncryptCode(encryptCode);
                if(map==null){
                    jsonObj.put("code","400");
                    jsonObj.put("desc","请求参数无效");
                }else{
                    jsonObj.put("data",map);
                    jsonObj.put("code","200");
                    jsonObj.put("desc","success");
                }
            }else{
                //没有合法的appKey
                jsonObj.put("code","407");
                jsonObj.put("desc","无效的appKey");
            }
            return RSAUtils.encrypt(jsonObj.toString(), ConstantUtil.PUBLIC_KEY_FINANCE);
        }catch (Exception e){
            jsonObj.put("code","500");
            jsonObj.put("desc","系统繁忙，请稍后");
            logger.error("快速登陆出现异常：" + e);
            return RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY_FINANCE);
        }
    }


    public static void main(String args[]){
        //公钥加密参数
        try {
            String message = RSAUtils.encrypt("niahosdf", ConstantUtil.PUBLIC_KEY_E).replaceAll("\\+", "%2B");
            System.out.println(message);
            message = "hE66BRU80doTlVEO2AM1qUsb+HfgNFGzTIxVIdARfep0prww+2dp8kBqHCC5RMrr3niDAHKEnKOrCH6JesopuPMcF0MHOO44OymqgUfbtt6evJKp+99zhTLpeSVlgQDhWZ+sX0jHvSgHaTkHJrFQrzPaCtPekbJdeKpnREEst2I=";
            //私钥解密数据
//            message = RSAUtils.decrypt(message, ConstantUtil.PRIVATE_KEY);
//            String encryptCode = ((Map<String, Object>)StringUtils.parseJSON2Map(message).get("params")).get("encryptCode").toString();
            String result = RSAUtils.decrypt(message, ConstantUtil.PRIVATE_KEY);
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
