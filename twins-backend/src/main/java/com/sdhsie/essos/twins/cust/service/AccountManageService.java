package com.sdhsie.essos.twins.cust.service;

import com.google.common.collect.Maps;
import com.sdhs.xlpayment.utils.encrypt.RSAUtils;
import com.sdhsie.essos.twins.base.ConstantUtil;
import com.sdhsie.essos.twins.util.KeysUtil;
import com.sdhsie.westeros.kingslanding.cust.model.CustDicking;
import com.sdhsie.westeros.kingslanding.cust.model.Customer;
import com.sdhsie.westeros.kingslanding.cust.service.CustDickingService;
import com.sdhsie.westeros.kingslanding.cust.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * Created by huangshuai on 2017/7/11.
 */

@Service
public class AccountManageService {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    CustDickingService custDickingService;
    @Autowired
    YunPianSmsService yunPianSmsService;
    @Autowired
    CustomerService customerService;

    /**
     * 通过手机号码为新用户注册
     */
    @Transactional("kingslandingTM")
    public Map<String,String> registerByMobilePhone(String mobilePhone, String wxKey, String aliPayKey, String source, String appKey){
        Map<String,String> m = Maps.newHashMap();
        //向用户信息表与相关表内插入信息
        Customer customer = new Customer();
        customer.setMobilePhone(mobilePhone);
        customer.setNickName("egs_" + mobilePhone);
        //生成随机密码
        String randomPwd = KeysUtil.createRandomPwd(appKey,customer.getId());
        customer.setLoginPassword(randomPwd);
        customerService.add(customer);
        //向cust_dicking表内插入信息
        CustDicking custDicking = new CustDicking();
        custDicking.setCustId(customer.getId());
        custDicking.setSource(source);
        if(wxKey != null && wxKey.length() > 0){
            custDicking.setWxKey(wxKey);
        }
        if(aliPayKey != null && aliPayKey.length() > 0){
            custDicking.setAliPayKey(aliPayKey);
        }
        custDicking.setAppKey(appKey);
        //encryptCode
        String encryptCode = KeysUtil.createEncryptCode(appKey,customerService.getByMobilePhone(mobilePhone).getId());
        if(custDickingService.isEncryptCode(encryptCode)){
            //有重复的，重新生成
            custDicking.setEncryptcode(KeysUtil.createEncryptCode(appKey,customerService.getByMobilePhone(mobilePhone).getId()));
        }else{
            custDicking.setEncryptcode(encryptCode);
        }
        custDickingService.add(custDicking);
        //将生成的随机密码，短信发送给用户
        yunPianSmsService.sendRandomPwd(mobilePhone,randomPwd);
        try {
            m.put("nickName",customer.getNickName());
            m.put("encryptCode",encryptCode);
        } catch (Exception e) {
            logger.error("加密异常：" + e);
            e.printStackTrace();
        }
        return m;
    }

    /**
     * 生成encryptcode，并将信息存进cust_docking表,并返回encryptcode
     */
    public String saveCustDocking(Long custId,String wxKey,String aliPayKey,String source,String appKey,String mobilePhone){
        CustDicking c = new CustDicking();
        if(appKey != null && appKey.length() > 0){
            c.setAppKey(appKey);
        }
        if(custId != null && String.valueOf(custId).length() > 0){
            c.setCustId(custId);
        }
        if(wxKey != null && wxKey.length() > 0){
            c.setWxKey(wxKey);
        }
        if(aliPayKey != null && aliPayKey.length() > 0){
            c.setAliPayKey(aliPayKey);
        }
        if(source != null && source.length() > 0){
            c.setSource(source);
        }
        String encryptCode = KeysUtil.createEncryptCode(appKey,customerService.getByMobilePhone(mobilePhone).getId());
        if(custDickingService.isEncryptCode(encryptCode)){
            //有重复的，重新生成
            c.setEncryptcode(KeysUtil.createEncryptCode(appKey,customerService.getByMobilePhone(mobilePhone).getId()));
        }else{
            c.setEncryptcode(encryptCode);
        }
        custDickingService.add(c);
        return encryptCode;
    }

}
