package com.sdhsie.essos.twins.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by huangshuai on 2017/5/17.
 * 各种加密算法
 */

public class EncryptionUtils {

    /**利用MD5进行加密
     * @param str  待加密的字符串
     * @return  加密后的字符串
     * @throws NoSuchAlgorithmException  没有这种产生消息摘要的算法
     * @throws UnsupportedEncodingException
     */
    public static String EncoderByMd5(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        //确定计算方法
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        // 计算md5函数
        md5.update(str.getBytes());
        return new BigInteger(1, md5.digest()).toString(32);
    }

    /**
     * 传入文本内容，返回 SHA-256 串
     * @param strText
     * @return
     */
    public static String SHA256(final String strText){
        return SHA(strText, "SHA-256");
    }

    /**
     * 传入文本内容，返回 SHA-512 串
     * @param strText
     * @return
     */
    public static String SHA512(final String strText){
        return SHA(strText, "SHA-512");
    }

    /**
     * 字符串 SHA 加密
     * @param strSourceText
     * @return
     */
    private static String SHA(final String strText, final String strType) {
        // 返回值
        String strResult = null;
        // 是否是有效字符串
        if (strText != null && strText.length() > 0) {
            try {
                //SHA 加密开始
                //创建加密对象 并传入加密型
                MessageDigest messageDigest = MessageDigest.getInstance(strType);
                //传入要加密的字符串
                messageDigest.update(strText.getBytes());
                //得到byte类型结果
                byte byteBuffer[] = messageDigest.digest();

                //将byte转换为string
                StringBuffer strHexString = new StringBuffer();
                //遍历byte buffer
                for (int i = 0; i < byteBuffer.length; i++)
                {
                    String hex = Integer.toHexString(0xff & byteBuffer[i]);
                    if (hex.length() == 1)
                    {
                        strHexString.append('0');
                    }
                    strHexString.append(hex);
                }
                // 得到返回结果
                strResult = strHexString.toString();
            }
            catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        return strResult;
    }
}
