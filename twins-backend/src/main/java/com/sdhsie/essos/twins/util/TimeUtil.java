package com.sdhsie.essos.twins.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by huangshuai on 2017/7/21.
 * 处理时间相关类
 */
public class TimeUtil {

    /*
     * 将时间戳转换为时间
     */
    public static String stampToDate(String s){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

    /**
     * 测试方法
     * @param arg
     */
    public static void main(String arg[]){
        System.out.println("===//:" + stampToDate("1500625200544"));
    }
}
