package com.sdhsie.essos.twins.cust.web;

import com.sdhsie.essos.twins.base.BaseAuthController;
import com.sdhsie.essos.twins.util.VerifyCodeUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * Created by huangshuai on 2017/7/10.
 * 验证码controller
 */

@Controller
@RequestMapping("/verify")
public class VerifyCodeController extends BaseAuthController {

    /**
     * 获取图片验证码
     * */
    @ResponseBody
    @RequestMapping(value = "/verifyCode")
    public void verifyCode() throws IOException {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        //生成随机字串
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
        //存入会话session
        HttpSession session = getRequest().getSession(true);
        session.setAttribute("verifyCode", verifyCode.toLowerCase());
        session.setAttribute("verifyCodeExpireTime", LocalDateTime.now().plusMinutes(3));
        logger.debug("SessionId:" + session.getId());
        logger.debug("verifyCode1:" + session.getAttribute("verifyCode"));
        logger.debug("verifyCodeExpireTime1:" + session.getAttribute("verifyCodeExpireTime"));
        //生成图片
        int w = 100, h = 40;
        VerifyCodeUtils.outputImage(w, h, response.getOutputStream(), verifyCode);
    }

}
