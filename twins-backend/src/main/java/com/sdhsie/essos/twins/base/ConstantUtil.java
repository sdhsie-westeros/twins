package com.sdhsie.essos.twins.base;

import com.sdhs.xlpayment.utils.encrypt.RSAUtils;

/**
 * Created by huangshuai on 2017/7/12.
 * 存放各种常量
 */

public class ConstantUtil {

    /**
     * 加密Demo
     * String str = RSAUtils.encrypt(params, PUBLIC_KEY);
     *
     * 解密Demo
     * String decrypt = RSAUtils.decrypt(str, PRIVATE_KEY);
     */

    /**
     * RSA公钥--无感支付信联公钥--加密
     */
    public static final String PUBLIC_KEY =
        //测试
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBoVj7iYHl2HMYduQu+SYd7MhZ42FrSwTdwgy10jjiQNmufUNCt/0xPzYvNgZdT7y6p6eYEPQDjoKK65EYstGTwTJv98tW1CAA1qneErxW13AIXJ6FeMbCMANJCDby9jdGXSDMBDn8Uq7Alya8doxPVX4GgbYPJPtg1wPOMA5CRQIDAQAB";
        //正式
//        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQClIxA4HPouOPLkPoXoHBlrVZyImaeEs+V8qnmyrLN6yAxlyIrt30oQD54SS38ixaKO5BN+CdynEcAFe9k1Y9bxQjUQXb62az2WMCPlgNfEvM5J9BWysbrJcUnIuvBrbHj6BnCRdxCKLXbDkijCz7NvNypZJyFrU0/L3FBE6PtLyQIDAQAB";



    /**
     * RSA公钥--车险分期金融公钥--加密finance
     */
    public static final String PUBLIC_KEY_FINANCE =
        //测试
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDf1AZWAejpf7/53mHxCiNXDvZ/3zJPCDn+bgThr4dSiCWgvkK+a5mUQ15HGMnN6IZOgcjqQKNIf3agmN/euZ3YFdFgSMWREmU4N+/Kqp0YGJ9ARgV1fOZb0I4vGVdk+w7cDR//g8TjNUOtHw9s9RckztUBq9y+X8KMowk5DXxaxQIDAQAB";
        //正式
//        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCymw/0pqqfL3YAi79+EozpVmOVMgrTiRAiKjbBs7FyqjS+Ka4apdY5wZR9dAzbVF31kLujitFDsJeVUNAK1ByFNSzGjjE54PYIPRCRQ9ComS5AHYRZV8z5g4jtdjQFiN6gu2CUFGeA3L2cqYpgIDx7qdksuCjmlM1VGRxlv6JM2QIDAQAB";



    /**
     * RSA私钥--e高速自己的私钥--加密
     */
    public static final String PRIVATE_KEY =
        //测试
        "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAPp2HAVXkLAKyZ+Q7vd4W/Ahcso42B2Fvjx0cLuRwoMIznKmnSek0phIG41/93ubFYdHgJKWqSsfSy/bmJYY+QBDU4qAWG8CD9uP9DtZ5ybfoze00RZXWbZVCfMRneIXzpVVNGTewHxKH/vsxHdZd69EpfK5JJLK9jJOeSghH4n5AgMBAAECgYAvYJoqVIcVFeVP3s2lWEoj4V/iauBOrVbv/qK4iHj/6nGK9INz1VTU44b6vEMnSY3H+jmTARbdBl1lnyd89Rpcl5Nf5Hrm/Ev0nlZmxqBLAKGLydD81LNg7ZX/wpCFPEx5YBJsUFui7WXuQsWH7OuttcQXDU3Qt7sm15e3wAhe5QJBAP2wjIpsHm0+xUNQEG5HMsqObc8/gmgJailaOUKQoZYj6I5fT/I0aU2MHtFxhWrtDPkYs2EEKINq/syjJcbQ6gcCQQD8vgi47UFLzIu7E/HSeLSF0dpTqMZ44XSFk/xKaPalBphyFpjCVtpSZw9VhTwD5ugQFVs8BkWIFARuGn+pvuv/AkAantUQjWnf8s9GUfd3avc0dB+nuLVZb2t5bWNGzeB23cvAbNX5x3ApWAO/3JawcrGCzgm8q3AI4khcdt4276MvAkAnuMYuKjuk6hTb5hqPdaEJlx21IEVxIh3qEIQKPczlys+SvrJreLL/WGs0iagRBO1b+b9JA8NRy7OWilueFf+JAkEA96hQGEdA2WFWI3EEMD9q/Wxyw4Ugv2U7kTj8lMR5HhFyO1dWMa1kCKX/332ISg7yQ5FtNlLQ /m7rTy5tSyCaow==";
        //生产
//        "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMYklC9LZ193ob9dvurxIVqO95Qd3wzj8YY2nxrPKn7BNjNP6kBVinLa1G5pyXZYR/uKHWBzsOpx6XkLnSSVz5ssJ4q2x8ydFsajj8Im313YabcCYmK2cMJXpwS/ZWcdPCwrH94tIS3nHNfP8hmkPvvHP8yhFO10Iv0xR4wPnMU7AgMBAAECgYAhH8RWE7j4ULuNjlJ4XE9DIFtrO4MqWIDr+v2mMbPc9NfmxBlZH/PBN6wDuVOgoSmHd5NpqG2PA/QP9E0U7KaD8xDecKxcputU3eLfn5hjvL5G/NDPrF1c/mywfmT/Re9AxBiPv0YmcohhmLwjN4Yu3F0K2VQknzv6cg7N2SLrAQJBAOVyMUPA4v0L0n7lBHVjT3QQxnoGY/XLCgja2/0yjgNrXTJOV8l/lmvdfFR+uWvpV/KjzIdPjIxn0LuOtW+cdaECQQDdEvdGeOidJVDU8pSC2Go9yvkPI+ejpifQ9F/Uq3jbambGISkdfQG8I7BSePwgj9ASxAWPR59ymxN1Cd/hINVbAkEA1c6ZVbeSTZcMqa6jfvfx/Af9y4vU7iAr06COSt0nNMyBZfVXHK9F7nHP/7Su+H3StIoqdxSLtfKvsXcEX2L+wQJBAM3KL+CsAGz4JYx024YJiqxG2x0a4S/XJc0/Dk3b6L7nBxB8IdfPW+QUnLfI43o0JQ6y5jIADA8VCEG0lXDtrW0CQE6klqdZIkBf3sfb8V1FpkVFtlPQNS9SCaZ3r2J1n850rOw39492vKwbp5y4a8+71WgrA5HaG9osB8aa0phAXCA=\n";
    /**
     * RSA公钥--e高速自己的私钥--加密
     */
    public static final String PUBLIC_KEY_E =
        //测试
        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQD6dhwFV5CwCsmfkO73eFvwIXLKONgdhb48dHC7kcKDCM5ypp0npNKYSBuNf/d7mxWHR4CSlqkrH0sv25iWGPkAQ1OKgFhvAg/bj/Q7Wecm36M3tNEWV1m2VQnzEZ3iF86VVTRk3sB8Sh/77MR3WXevRKXyuSSSyvYyTnkoIR+J+QIDAQAB";
        //生产
//        "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDGJJQvS2dfd6G/Xb7q8SFajveUHd8M4/GGNp8azyp+wTYzT+pAVYpy2tRuacl2WEf7ih1gc7Dqcel5C50klc+bLCeKtsfMnRbGo4/CJt9d2Gm3AmJitnDCV6cEv2VnHTwsKx/eLSEt5xzXz/IZpD77xz/MoRTtdCL9MUeMD5zFOwIDAQAB";


    /**
     * 测试方法
     * @param str
     * @throws Exception
     */
    public static void main(String str[]) throws Exception {
//        System.out.println("解密：" + RSAUtils.decrypt("no+b/eWEIQ3/Ot9QV3kOHkKaDxmhjgj86WrwO2Ehg9+I/7cilwfimfRil+KZFMe+hb8Msm0uk1tYpsc4mdaKp/aopM8MjpRkZeLTFxU5QtTJ2awCnEekcZsY+HJOOC+xEJijjfpgQ18awYpF+1Z9YRQpYb4vmsozo8FKiR7EOR8=", PRIVATE_KEY));
        System.out.println(RSAUtils.encrypt("{\"mobilePhone\":\"18766590265\",\"wxKey\":\"\",\"aliPayKey\":\"2088502194867220\",\"source\":\"ali\"}".replaceAll("\\+", "%2B"), PUBLIC_KEY));
    }

}
