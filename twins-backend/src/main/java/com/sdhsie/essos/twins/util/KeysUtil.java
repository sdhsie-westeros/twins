package com.sdhsie.essos.twins.util;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by huangshuai on 2017/7/10.
 * 生成Key，验证Key
 */
public class KeysUtil {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMddHHmm");

    /**
     * 生成6位随机密码
     * 规则：appKey加上custId，然后SHA256算法加密，再截取6位
     * @param appKey
     * @param custId
     * @return
     */
    public static String createRandomPwd(String appKey,Long custId){
        return EncryptionUtils.SHA256(appKey + custId).substring(0,6);
    }

    /**
     * 生成encryptcode，
     * 规则：appKey加上custId，然后SHA256算法加密，再截取32位
     */
    public static String createEncryptCode(String appKey,Long custId){
        return EncryptionUtils.SHA256(appKey + custId).substring(0,32);
    }

    /**
     * 规则：appKey加上custId，然后SHA256算法加密，再截取32位
     * @param appKey
     * @param custId
     * @return
     */
    public static String createEncryptCode512(String appKey,Long custId){
        return EncryptionUtils.SHA512(appKey + custId).substring(0,32);
    }

    /**
     * 生成appKey
     * 规则：公司拼音代码加md5加密后的时间戳截取10位
     */
    public static String createAppKey(String pinYin) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return pinYin + EncryptionUtils.EncoderByMd5(dateFormat.format(new Date())).substring(0,10);
    }

}
