package com.sdhsie.essos.twins.cust.web;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.common.collect.Maps;
import com.sdhs.xlpayment.utils.encrypt.RSAUtils;
import com.sdhsie.essos.twins.base.BaseAuthController;
import com.sdhsie.essos.twins.base.ConstantUtil;
import com.sdhsie.essos.twins.cust.service.AccountManageService;
import com.sdhsie.essos.twins.util.StringUtils;
import com.sdhsie.westeros.kingslanding.cust.model.CustDicking;
import com.sdhsie.westeros.kingslanding.cust.model.Customer;
import com.sdhsie.westeros.kingslanding.cust.service.AppCooperationService;
import com.sdhsie.westeros.kingslanding.cust.service.CustDickingService;
import com.sdhsie.westeros.kingslanding.cust.service.CustomerService;
import com.sun.org.apache.xpath.internal.SourceTree;
import net.sf.json.JSONObject;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by huangshuai on 2017/7/11.
 * 根据要求修改后的账户管理controller
 */

@Controller
@RequestMapping("/account")
public class AccountManageController extends BaseAuthController {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    CustomerService customerService;
    @Autowired
    AppCooperationService appCooperationService;
    @Autowired
    AccountManageService accountManageService;
    @Autowired
    CustDickingService dickingService;

    /**
     * 登陆或注册方法
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/registerOrLogin", params = {"params"}, method = {RequestMethod.POST})
    public Object registerOrLogin(String params,HttpServletRequest request){
        Map<String,String> m = Maps.newHashMap();
        JSONObject jsonObj = new JSONObject();
        String result = "";
        logger.error("appKey："+request.getHeader("appKey"));
        logger.error("params："+params);
        try{
            //获取header中的appKey，首先需要先对齐进行解密
            String appKey = RSAUtils.decrypt(request.getHeader("appKey"), ConstantUtil.PRIVATE_KEY);
            //首先验证注册用户是否有appKey
            if(appCooperationService.checkAppKey(appKey)){
                //解密手机号
//                String newMobilePhone = RSAUtils.decrypt(mobilePhone,ConstantUtil.PRIVATE_KEY);
                String newMobilePhone = StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY)).get("mobilePhone").toString();
                //有合法的appKey，可以进行注册操作，先看是否有该用户
                Customer customer = customerService.getByMobilePhone(newMobilePhone);
                Map<String,Object> paramsMap = new HashedMap();
                paramsMap = StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY));
                String wxKey = "";
                String aliPayKey = "";
                String source = "";
                if(paramsMap.get("wxKey") != null){
                    wxKey = paramsMap.get("wxKey").toString();
                }
                if(paramsMap.get("aliPayKey") != null){
                    aliPayKey = paramsMap.get("aliPayKey").toString();
                }
                if(paramsMap.get("source") != null){
                    source = paramsMap.get("source").toString();
                }
                logger.error("wxKey：" + wxKey);
                logger.error("aliPayKey：" + aliPayKey);
                logger.error("source：" + source);
//                String wxKey = StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY)).get("wxKey").toString();
//                String aliPayKey = StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY)).get("aliPayKey").toString();
//                String source = StringUtils.parseJSON2Map(RSAUtils.decrypt(params,ConstantUtil.PRIVATE_KEY)).get("source").toString();
                if(customer != null){
                    //已注册用户，先看看是否有encryptcode
                    CustDicking custDicking = dickingService.getByCustId(customer.getId());
                    if(custDicking == null){
                        //没有encryptcode，需要生成，并进行存储
                        String encryptcode = accountManageService.saveCustDocking(customer.getId(),wxKey,aliPayKey, source,appKey, newMobilePhone);
                        //已经有encryptcode，可以直接返回
                        m.put("encryptCode", encryptcode);
                        m.put("nickName",customer.getNickName());
                    }else{
                        //已经有encryptcode，可以直接返回
                        m.put("encryptCode",custDicking.getEncryptcode());
                        m.put("nickName",customer.getNickName());
                    }

                }else{
                    //未注册用户，首先需要进行注册操作
                    m = accountManageService.registerByMobilePhone(newMobilePhone, wxKey, aliPayKey, source, appKey);
                }
                jsonObj.put("data",m);
                jsonObj.put("code","200");
                jsonObj.put("desc","登陆成功");
                result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
                return result;
            }else{
                //没有合法的appKey
                jsonObj.put("code","407");
                jsonObj.put("desc","无效的Key");
                jsonObj.put("data","");
                result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
                return result;
            }
        }catch (Exception e){
            jsonObj.put("code","500");
            jsonObj.put("desc","系统繁忙，请稍后");
            jsonObj.put("data","");
            logger.error("快速登陆出现异常：" + e);
            try {
                result = RSAUtils.encrypt(jsonObj.toString(),ConstantUtil.PUBLIC_KEY);;
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return result;
        }
    }
}
